#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "ws2812.pio.h"
#include "hardware/watchdog.h"

#define IS_RGBW true        // Will use RGBW format
#define NUM_PIXELS 1        // There is 1 WS2812 device in the chain
#define WS2812_PIN 28       // The GPIO pin that the WS2812 connected to

absolute_time_t startTime;
char * input = "";
int flag = 0;
void main_asm();
int seed;

// Initialise a GPIO pin – see SDK for detail on gpio_init()
void asm_gpio_init(uint pin) {
    gpio_init(pin);
}

// Set direction of a GPIO pin – see SDK for detail on gpio_set_dir()
void asm_gpio_set_dir(uint pin, bool out) {
    gpio_set_dir(pin, out);
}

// Get the value of a GPIO pin – see SDK for detail on gpio_get()
bool asm_gpio_get(uint pin) {
    return gpio_get(pin);
}

// Set the value of a GPIO pin – see SDK for detail on gpio_put()
void asm_gpio_put(uint pin, bool value) {
    gpio_put(pin, value);
}

// Enable falling-edge interrupt – see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_set_irq(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL | GPIO_IRQ_EDGE_RISE, true);
}

void incrementFlag() {
    if(flag == 0)
        flag = 1;
    else if(flag == -1)
        flag = 2;
}

struct player // the struct that represents the player
{
    int currentRound; // the round the player is on
    int roundReached; // needed to account for when the player is selecting a level
    int numberOfLives;  //  the number of lives a player has left
    int numberOfWins; // the number of wins the player got
    int totalCorrectAnswers;
    bool gameFinished; // a flag to indicate whether the game is over or not
};

void playerReset(struct player * player) {
    player->currentRound = 0;
    player->roundReached = 0;
    player->numberOfLives = 3;
    player->numberOfWins = 0;
    player->totalCorrectAnswers = 0;
    player->gameFinished = false;
}

/**
 * @brief Function to print the players endgame statistics
 */
void printPlayerStats(struct player* myPlayer) // function to print out the stats for a player
{
    printf("Round reached: %d\n", myPlayer->roundReached); // first printing out the round
    //the player is on
    printf("Lives left: %d\n", myPlayer->numberOfLives); // then how many lives
    //the player has left
    printf("Games won: %d\n", myPlayer->numberOfWins); // then how many games were won
    printf("Total correct answers: %d\n", myPlayer->totalCorrectAnswers); // then how many games were won
}

struct letter {
    char letter; // char representation of letter
    char *morse_code; // morse code representation of letter, in binary string format
};

// Creates an instance of letter.
struct letter letter_create(char letter, char *morse_code) {
    struct letter output;
    output.letter = letter;
    output.morse_code = morse_code;
    return output;
}

struct letter letter_array[37];
void letter_array_create() {
    letter_array[0] = letter_create('A',".-");
    letter_array[1] = letter_create('B',"-...");
    letter_array[2] = letter_create('C',"-.-.");
    letter_array[3] = letter_create('D',"-..");
    letter_array[4] = letter_create('E',".");
    letter_array[5] = letter_create('F',"..-.");
    letter_array[6] = letter_create('G',"--.");
    letter_array[7] = letter_create('H',"....");
    letter_array[8] = letter_create('I',"..");
    letter_array[9] = letter_create('J',".---");
    letter_array[10] = letter_create('K',"-.-");
    letter_array[11] = letter_create('L',".-..");
    letter_array[12] = letter_create('M',"--");
    letter_array[13] = letter_create('N',"-.");
    letter_array[14] = letter_create('O',"---");
    letter_array[15] = letter_create('P',".--.");
    letter_array[16] = letter_create('Q',"--.-");
    letter_array[17] = letter_create('R',".-.");
    letter_array[18] = letter_create('S',"...");
    letter_array[19] = letter_create('T',"-");
    letter_array[20] = letter_create('U',"..-");
    letter_array[21] = letter_create('V',"...-");
    letter_array[22] = letter_create('W',".--");
    letter_array[23] = letter_create('X',"-..-");
    letter_array[24] = letter_create('Y',"-.--");
    letter_array[25] = letter_create('Z',"--..");
    letter_array[26] = letter_create('0',"-----");
    letter_array[27] = letter_create('1',".----");
    letter_array[28] = letter_create('2',"..---");
    letter_array[29] = letter_create('3',"...--");
    letter_array[30] = letter_create('4',"....-");
    letter_array[31] = letter_create('5',".....");
    letter_array[32] = letter_create('6',"-....");
    letter_array[33] = letter_create('7',"--...");
    letter_array[34] = letter_create('8',"---..");
    letter_array[35] = letter_create('9',"----.");
    letter_array[36] = letter_create('?',"..--..");
}

// Returns a specific character from a letter's morse code
// string, based on an index. Useful for reading in
// individual characters.
char letter_code_index(struct letter *ltr, int index) {
    return ltr->morse_code[index];
}

// Returns a letter based on a char letter input.
struct letter get_letter(char letter) {
    int arr_index = (int)letter - 65;
    return letter_array[arr_index];
}

// Compares an existing letter with a morse string input.
int letter_compare(struct letter *ltr, char *morse) {
    if (strcmp(ltr->morse_code, morse) == 0) {
        return 1;
    }
    return 0;
}

/**
 * @brief Function to print Welcome Message for the game
 */
void welcomeMessage(){
    printf("\n");
    printf("           Welcome to Codebreaker!           \n");
    printf("                                             \n");
    printf("                                             \n");
    printf("                How to Play:                 \n");
    printf("    -Use the GP21 Button to provide input    \n");
    printf("           -Press < X.Xs = Dot / '.'         \n");
    printf("          -Press > X.Xs = Dash / '-'         \n");
    printf("         -No input for 3s = Submit           \n");
    printf("                                             \n");
    printf("         GET 5 IN A ROW TO PROGRESS          \n");
    printf("             TO THE NEXT LEVEL               \n"); 
    printf("                                             \n");
    printf("                Level Select:                \n");
    printf("               1    =    .----               \n");
    printf("               2    =    ..---               \n");
    printf("               3    =     TBA                \n");
    printf("               4    =     TBA                \n");
}

/**
 * @brief Function to set the global variable startTime to the current time since launch
 */
void startTimer(){
    startTime = get_absolute_time();
}

/**
 * @brief Function to return the total time difference in microseconds between startTime and time at function call
 */
int endTimer(){
    uint64_t diff = absolute_time_diff_us(startTime, get_absolute_time());
    // Use length of button press as seed for random generation
    seed = diff;
    return (int)diff;
}


/**
 * @brief Wrapper function used to call the underlying PIO
 *        function that pushes the 32-bit RGB colour value
 *        out to the LED serially using the PIO0 block. The
 *        function does not return until all of the data has
 *        been written out.
 *
 * @param pixel_grb The 32-bit colour value generated by urgb_u32()
 */
static inline void put_pixel(uint32_t pixel_grb) {
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}


/**
 * @brief Function to generate an unsigned 32-bit composit GRB
 *        value by combining the individual 8-bit paramaters for
 *        red, green and blue together in the right order.
 *
 * @param r     The 8-bit intensity value for the red component
 * @param g     The 8-bit intensity value for the green component
 * @param b     The 8-bit intensity value for the blue component
 * @return uint32_t Returns the resulting composit 32-bit RGB value
 */
static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b) {
    return  ((uint32_t) (r) << 8)  |
            ((uint32_t) (g) << 16) |
            (uint32_t) (b);
}

void updateLED(struct player * player) {
    if(player->currentRound != 0){
        switch(player->numberOfLives){
            case 3:
                // Put green at low intensity
                put_pixel(urgb_u32(0x0,0x3F,0x0));
                break;
            case 2:
                // Put yellow at low intensity
                put_pixel(urgb_u32(0x3F,0x3F,0x0));
                break;
            case 1:
                // Put red at low intensity
                put_pixel(urgb_u32(0x3F,0x0,0x0));
                break;
            default:
                break;
        }
    }
    else{
        // Put blue at low intensity
        put_pixel(urgb_u32(0x0,0x0,0x3F));
    }
}

void addChar(int x){
    char dot = '.';
    char dash = '-';
    char sp = ' ';
    char * temp = malloc((strlen(input)+1)*sizeof(char));
    strcpy(temp, input);
    strncat(temp,((x == 0)? &dot:(x == 1)?&dash:&sp),1);
    if(x == -1) {
        printf("SPACE");
        flag = -1;
    }
    input = temp;
}

/**
 * @brief Method for when player is inputting current answer
 *
 * @return bool indicating validity of input
 */
void playerInput(struct player * player, struct letter l){
    while(strcmp(input, l.morse_code) != 0 && strlen(input) < 5 && flag < 2) {
        sleep_ms(100);

        if(player->currentRound == 0)
            flag = 0;
        
        if(player->currentRound < 3 && flag == 1){
            watchdog_update();
            flag = 0;
            return;
        }
        /*else if(player->currentRound >= 3 && flag == 1) {
            watchdog_update();
            addChar((int)-1); //use -1 as space indicator for addChar
        }*/
    }
    flag = 0;
    return;
}

/**
 * @brief EXAMPLE - WS2812_RGB
 *        Simple example to initialise the NeoPixel RGB LED on
 *        the MAKER-PI-PICO and then flash it in alternating
 *        colours between red, green and blue forever using
 *        one of the RP2040 built-in PIO controllers.
 *
 * @return int  Application return code (zero for success).
 */
void playLevel(struct player* player) {
    int correctAnswerStreak = 0;
    printf("\nLevel %d Start!\n", player->currentRound);
    bool isCorrect;
    //update the colour of the LED based on the player's lives
    updateLED(player);
    while(correctAnswerStreak != 5 && player->numberOfLives != 0){
        //Reset input
        input = "";
        flag = 0;
        //If correct, correctAnswers and numberOfLives +1 (up to 3 lives)
        //If incorrect, numberOfLives--

        isCorrect = false;
        //produce a random letter from our letter_array (0 to 37)
        struct letter l;
        srand(seed);
        int index = rand() % 36;

        switch(player->currentRound){
            case 1:
                l = letter_array[index];
                //print a line of text giving the player their letter AND the morse code variant (see the letter struct for reference)
                printf("\nType the morse code for the letter\n'%c' -> \"%s\"\n", l.letter, l.morse_code);
                playerInput(player, l);
                if(strcmp(input, l.morse_code) == 0)
                    isCorrect = true; 
                break;
            case 2:
                l = letter_array[index];
                //print a line of text giving the player their letter AND the morse code variant (see the letter struct for reference)
                printf("\nType the morse code for the letter\n'%c'\n", l.letter);
                playerInput(player, l);
                if(strcmp(input, l.morse_code) == 0)
                    isCorrect = true; 
                break;
            case 3:
                //print a line of text giving the player their word AND the morse code variant (see the letter struct for reference)
                break;
            case 4:
                //print a line of text giving the player JUST their word
                break;
            default:
                printf("Something went wrong. Sorry!\n");
                break;
        }

        
        if(isCorrect) {
            printf("\nCorrect! \"%s\" is the morse code for '%c'\n", input, l.letter);
            if(player->numberOfLives != 3)
                player->numberOfLives++;
            correctAnswerStreak++;
            player->totalCorrectAnswers++;
        }

        else {
            printf("\nOh no, that's not right. The morse code for '%c' is \"%s\". Try Again!\n", l.letter, l.morse_code);
            correctAnswerStreak = 0;
            player->numberOfLives--;
        }

        printf("\nCurrent Streak: %d\n", correctAnswerStreak);
        //update the colour of the LED based on the player's lives
        updateLED(player);
        watchdog_update();
    }
    if(player->numberOfLives == 0 || player->currentRound == 2)
        player->gameFinished = true;
}

void levelSelect(struct player * player) {
    bool validInput = false;
    struct letter null = letter_create(' ', " ");
    printf("Select a level from above: ");
    while(!validInput){
        input = "";
        playerInput(player, null);

        // Level 1
        if(strcmp(input, ".----") == 0){
            player->currentRound = 1;
            validInput = true;
        }

        // Level  2
        else if(strcmp(input, "..---") == 0){
            player->currentRound = 2;
            validInput = true;
        }

        /*else if(strcmp(input, "...--") == 0){
            
        }*/

        /*else if(strcmp(input, "....-") == 0){
            
        }*/
        
        else {
            printf("\nInvalid Input. Try again!\n");
        }
    }
}


/**
 * @brief EXAMPLE - WS2812_RGB
 *        Simple example to initialise the NeoPixel RGB LED on
 *        the MAKER-PI-PICO and then flash it in alternating
 *        colours between red, green and blue forever using
 *        one of the RP2040 built-in PIO controllers.
 *
 * @return int  Application return code (zero for success).
 */
int main() {
    // Initialise all STDIO as we will be using the GPIOs
    stdio_init_all();

    // Initialise the array of letters
    letter_array_create();

    // Initialise the PIO interface with the WS2812 code
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);

    // Enable Watchdog timer
    watchdog_enable(8300,1); //when this is set off, app will reset

    if (watchdog_caused_reboot())
    {
        printf("Rebooted by Watchdog!\n");
    }

    struct player player = {0,0,3,0,0,false};

    // Do forever...
    while(true) {
        updateLED(&player);

        // Call to ASM main
        main_asm();

        // Display welcome message
        welcomeMessage();

        levelSelect(&player);
        // Start game and continue until all levels complete or player runs out of lives
        while(player.numberOfLives != 0 && player.gameFinished != true){
            input = "";
            // Play the game
            playLevel(&player);  // Runs the current level for the player and exits when the level is complete

            if(player.numberOfLives != 0){
                printf("\nLevel %d complete!\n", player.currentRound);
                player.numberOfWins++;
                // Show player stats
                printPlayerStats(&player);
                if(player.currentRound <= 2){
                    printf("\nMoving to level %d\n", ++player.currentRound);
                    player.numberOfLives = 3;
                    if(player.currentRound > player.roundReached)
                        player.roundReached = player.currentRound;
                }
            }
        }

        // Player lose message
        if(player.numberOfLives == 0)
            printf("\nSorry! Try again!\n");

        else
            printf("\nYou Won! Well done!\n");

        // Show player stats
        printPlayerStats(&player);

        playerReset(&player);

        // Wait 5 seconds and allow player to view statistics before game reset
        sleep_ms(5000);
    }

    // Should never get here due to infinite while-loop.
    return 0;

}

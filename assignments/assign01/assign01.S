#include "hardware/regs/addressmap.h"
#include "hardware/regs/io_bank0.h"
#include "hardware/regs/timer.h"
#include "hardware/regs/m0plus.h"

/*
Useful Register Locations:
TIMER_BASE - Base address for the RP2040 timer component.
TIMER_INTE_OFFSET - Offset address for the TIMER interrupt enable register.
TIMER_INTR_OFFSET - Offset address for the TIMER raw interrupts register.
TIMER_TIMELR_OFFSET - Offset address for the lower 32-bits of the timer register.
TIMER_ALARM0_OFFSET - Offset address for the ALARM0 control register.
PPB_BASE - Base address for the RP2040 VTOR and NVIC.
M0PLUS_VTOR_OFFSET - Offset address for the vector table.
M0PLUS_NVIC_ICPR_OFFSET - Offset address for the NVIC Interrupt Clear Pending register.
M0PLUS_NVIC_ISER_OFFSET - Offset address for the NVIC Interrupt Set Enable register.
IO_BANK0_BASE - Base address for the RP2040 GPIO component.
IO_BANK0_INTR2_OFFSET - Offset address for the GPIO raw interrupts #2 register.
IO_BANK0_PROC0_INTS2_OFFSET - Offset address for the GPIO interrupt status #2 register.
*/

.syntax unified
.cpu    cortex-m0plus
.thumb
.global main_asm
.align  4

.equ    DFLT_STATE_STRT, 1            @ Specify the value to start flashing
.equ    DFLT_STATE_STOP, 0            @ Specify the value to stop flashing
.equ    DFLT_ALARM_TIME, 1000000      @ Specify the default alarm timeout

.equ    GPIO_BTN_DN_MSK, 0x00040000   @ Bit-18 for falling-edge event on GP20
.equ    GPIO_BTN_EN_MSK, 0x00400000   @ Bit-22 for falling-edge event on GP21
.equ    GPIO_BTN_UP_MSK, 0x04000000   @ Bit-26 for falling-edge event on GP22

.equ    GPIO_BTN_DN,  20              @ Specify pin for the "down" button
.equ    GPIO_BTN_EN,  21              @ Specify pin for the "enter" button
.equ    GPIO_BTN_UP,  22              @ Specify pin for the "up" button
.equ    GPIO_LED_PIN, 25              @ Specify pin for the built-in LED
.equ    GPIO_DIR_IN,   0              @ Specify input direction for a GPIO pin
.equ    GPIO_DIR_OUT,  1              @ Specify output direction for a GPIO pin

.equ    LED_VALUE_ON, 1         @ Specify the value that turns the LED "on"
.equ    LED_VALUE_OFF, 0        @ Specify the value that turns the LED off"

.equ    LED_VAL_ON,    1              @ Specify value that turns the LED "on"
.equ    LED_VAL_OFF,   0              @ Specify value that turns the LED "off"

.equ    GPIO_ISR_OFFSET, 0x74         @ GPIO is int #13 (vector table entry 29)
.equ    ALRM_ISR_OFFSET, 0x40         @ ALARM0 is int #0 (vector table entry 16)

@ Personal Registers:
@   R4 = toggleStatus (BOOLEAN)
@   R5 = Alarm Delay (INT)

@ Entry point to the ASM portion of the program
main_asm:
    BL      init_led                @ Initialise LED Pin
    BL      init_btn_dn             @ Initialise GPIO 20
    BL      init_btn_en             @ Initialise GPIO 21
    BL      init_btn_up             @ Initialise GPIO 22
    BL      install_alarm_intr      @ Initialise Alarm Interrupt
    BL      install_btn_intr        @ Initialise GPIO Interrupt
    LDR     R4, =DFLT_STATE_STRT    @ Set R4 to Default Start State (1/Enabled)
    LDR     R5, =DFLT_ALARM_TIME    @ Set R5 to Default Alarm Delay
main_loop:
    BL      set_alarm_delay         @ Update Alarm Delay value
    WFI                             @ Wait for interrupt signal
    B       main_loop               @ Loop back to top

@-----------------------------------------------------------------------------------------------------------------------

init_led:
@ Initialise LED Pin
    PUSH    {LR}
    MOVS    R0, #GPIO_LED_PIN       @ This value is the GPIO LED pin on the PI PICO board
    BL      asm_gpio_init           @ Call the subroutine to initialise the GPIO pin specified by R0
    MOVS    R0, #GPIO_LED_PIN       @ This value is the GPIO LED pin on the PI PICO board
    MOVS    R1, #GPIO_DIR_OUT       @ We want this GPIO pin to be setup as an output pin
    BL      asm_gpio_set_dir        @ Call the subroutine to set the GPIO pin specified by R0 to state specified by R1
    POP     {PC}

@-----------------------------------------------------------------------------------------------------------------------

init_btn_dn:
@ Initialise GPIO 20 - Down Button
    PUSH    {LR}
    MOVS    R0, #GPIO_BTN_DN        @ This value is the GPIO 20 pin on the PI PICO board
    BL      asm_gpio_init           @ Call the subroutine to initialise the GPIO pin specified by R0
    MOVS    R0, #GPIO_BTN_DN        @ This value is the GPIO 20 pin on the PI PICO board
    MOVS    R1, #GPIO_DIR_IN        @ We want this GPIO pin to be setup as an input pin
    BL      asm_gpio_set_dir        @ Call the subroutine to set the GPIO pin specified by R0 to state specified by R1
    MOVS    R0, #GPIO_BTN_DN        @ This value is the GPIO 20 pin on the PI PICO board
    BL      asm_gpio_set_irq        @ Call the subroutine to enable falling edge interrupts for this pin
    POP     {PC}

@-----------------------------------------------------------------------------------------------------------------------

init_btn_en:
@ Initialise GPIO 21 - Enter Button
    PUSH    {LR}
    MOVS    R0, #GPIO_BTN_EN        @ This value is the GPIO 21 pin on the PI PICO board
    BL      asm_gpio_init           @ Call the subroutine to initialise the GPIO pin specified by R0 
    MOVS    R0, #GPIO_BTN_EN        @ This value is the GPIO 21 pin on the PI PICO board
    MOVS    R1, #GPIO_DIR_IN        @ We want this GPIO pin to be setup as an input pin
    BL      asm_gpio_set_dir        @ Call the subroutine to set the GPIO pin specified by R0 to state specified by R1
    MOVS    R0, #GPIO_BTN_EN        @ This value is the GPIO 21 pin on the PI PICO board
    BL      asm_gpio_set_irq        @ Call the subroutine to enable falling edge interrupts for this pin
    POP     {PC}

@-----------------------------------------------------------------------------------------------------------------------

init_btn_up:
@ Initialise GPIO 22 - Up Button
    PUSH    {LR}
    MOVS    R0, #GPIO_BTN_UP        @ This value is the GPIO 22 pin on the PI PICO board
    BL      asm_gpio_init           @ Call the subroutine to initialise the GPIO pin specified by R0 
    MOVS    R0, #GPIO_BTN_UP        @ This value is the GPIO 22 pin on the PI PICO board
    MOVS    R1, #GPIO_DIR_IN        @ We want this GPIO pin to be setup as an input pin
    BL      asm_gpio_set_dir        @ Call the subroutine to set the GPIO pin specified by R0 to state specified by R1
    MOVS    R0, #GPIO_BTN_UP        @ This value is the GPIO 22 pin on the PI PICO board
    BL      asm_gpio_set_irq        @ Call the subroutine to enable falling edge interrupts for this pin
    POP     {PC}

@-----------------------------------------------------------------------------------------------------------------------

install_alarm_intr:
    PUSH    {LR}

    @ Add ISR address to vector table
    LDR     R0, =(PPB_BASE + M0PLUS_VTOR_OFFSET)            @ This is the base address for the Vector Table
    LDR     R1, [R0]                                        @ Load the value from this address into R1
    MOVS    R0, #ALRM_ISR_OFFSET                            @ Offset to Alarm0 in the Vector Table
    ADD     R0, R1                                          @ Add this offset to the Vector Table value
    LDR     R1, =toggle_led                                 @ Get the address of the ISR
    STR     R1, [R0]                                        @ Store the address of the ISR in the position of Alarm0 in the Vector Table

    @ Disable interrupt for Alarm
    LDR     R0, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)       @ Address of the NVIC Clear-Pending Register
    MOVS    R1, #1                                          @ Alarm0 is int #0, so set R1 to 1
    STR     R1, [R0]                                        @ Set appropriate bit to 1 to disable the interrupt

    @ Enable interrupt for Alarm
    LDR     R0, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)       @ Address of the NVIC Set-Enable Register
    MOVS    R1, #1                                          @ Alarm0 is int #0, so set R1 to 1
    STR     R1, [R0]                                        @ Set appropriate bit to 1 to enable the interrupt

    POP     {PC}

@-----------------------------------------------------------------------------------------------------------------------

@ Install button interrupt
install_btn_intr:
    PUSH    {LR}

    @ Add ISR address to vector table
    LDR     R0, =(PPB_BASE + M0PLUS_VTOR_OFFSET)        @ This is the base address of the Vector Table
    LDR     R1, [R0]                                    @ Load the value from this address into R1
    MOVS    R0, #GPIO_ISR_OFFSET                        @ Offset to GPIO in the Vector Table
    ADD     R0, R1                                      @ Add this offset to the Vector Table value
    LDR     R1, =gpio_isr                               @ Get the address of the ISR
    STR     R1, [R0]                                    @ Store the address of the ISR in the position of GPIO in the Vector Table

    @ Disable interrupt for Alarm
    LDR     R0, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)   @ Address of the NVIC Clear-Pending Register
    MOVS    R1, #1                                      @ Set R1 to 1 for enable bit
    LSLS    R1, #13                                     @ GPIO is int #13, so shift R1 left accordingly to mask
    STR     R1, [R0]                                    @ Set the appropriate bit to 1 to disable the interrupt

    @ Enable interrupt for Alarm
    LDR     R0, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)   @ Address of the NVIC Set-Enable Register
    MOVS    R1, #1                                      @ Set R1 to 1 for enable bit
    LSLS    R1, #13                                     @ GPIO is int #13, so shift 1 left accordingly to mask
    STR     R1, [R0]                                    @ Set the appropriate bit to 1 to disable the interrupt
    
    POP     {PC}
@-----------------------------------------------------------------------------------------------------------------------

@ Set alarm delay
set_alarm_delay:
    PUSH    {LR}

    @ Enable alarm
    LDR     R0, =(TIMER_BASE + TIMER_INTE_OFFSET)       @ Address of the Timer Interrupt Enabler
    STR     R4, [R0]                                    @ Store value of toggleStatus (R4) in here (either on or off)
    @ Get timer value                           
    LDR     R0, =(TIMER_BASE + TIMER_TIMELR_OFFSET)     @ This is where the current value of the alarm delay is found
    LDR     R1, [R0]                                    @ Get the current remaining value of the alarm delay
    ADD     R1, R5                                      @ Add Alarm Delay (R5) to the remaining alarm delay
    @ Update alarm delay value                                
    LDR     R0, =(TIMER_BASE + TIMER_ALARM0_OFFSET)     @ This is where you set the alarm delay
    STR     R1, [R0]                                    @ Store the updated alarm delay here

    POP     {PC}                                   

@-----------------------------------------------------------------------------------------------------------------------

@ GPIO interrupt
.thumb_func 
gpio_isr:
    PUSH    {LR}

    @ Handles each button press internally
    LDR     R0, =(IO_BANK0_BASE + IO_BANK0_PROC0_INTS2_OFFSET)  @ This is where the interrupt status can be found (Where the interrupt came from)
    LDR     R1, [R0]                                            @ Load the value from this address
    LDR     R0, =GPIO_BTN_DN_MSK                                @ This is the mask for if GPIO 20 set off the interrupt
    CMP     R0, R1                                              @ If GPIO 20 set off the interrupt:
    BEQ     btn_dn                                              @       Branch to btn_dn
    LDR     R0, =GPIO_BTN_UP_MSK                                @ This is the mask for if GPIO 22 set off the interrupt
    CMP     R0, R1                                              @ Else if GPIO 22 set of the interrupt:
    BEQ     btn_up                                              @       Branch to btn_up

    btn_en:                                                     @ Else (GPIO 21 set off the interrupt):
    MOVS    R0, #1                                              @ Set R0 to 1 for enable
    EORS    R4, R0                                              @ Switch the value of toggleStatus (R4)
    CMP     R4, #DFLT_STATE_STRT                                @ If (toggleStatus):
    BEQ     led_on_msg                                          @       set message to LED flashing message
    LDR     R0, =not_flashing_msg                               @ Else: set message to LED not flashing message
    B       end_btn_en                                          @ Branch to end
    led_on_msg:
    LDR     R0, =flashing_msg
    end_btn_en:
    BL      printf                                              @ Print given message from R0
    B       end_gpio

btn_dn:
    @ If flashing, half timer, else reset delay
    CMP     R4, #DFLT_STATE_STOP                                @ If not flashing:
    BEQ     reset_delay                                         @    Branch to reset_delay
    LSRS    R5, #1                                              @ Shift alarm delay right by 1 to achieve division by 2
    LDR     R0, =half_msg                                       @ Set message to halving messsage
    BL      printf                                              @ Print message from R0
    B       end_gpio                

btn_up:
    @ If flashing, double timer, else reset delay
    CMP     R4, #DFLT_STATE_STOP                                @ If not flashing:
    BEQ     reset_delay                                         @    Branch to reset_delay
    LSLS    R5, #1                                              @ Shift alarm delay right by 1 to achieve division by 2
    LDR     R0, =double_msg                                     @ Set message to halving messsage
    BL      printf                                              @ Print message from R0
    B       end_gpio

reset_delay:
    LDR     R0, =reset_msg                                      @ Set message to reset message
    BL      printf                                              @ Print message from R0
    LDR     R5, =DFLT_ALARM_TIME                                @ Set R5 to the Default Alarm Delay

end_gpio:
    @ Clear pending interrupt status using passed button mask in R0
    LDR     R0, =(IO_BANK0_BASE + IO_BANK0_PROC0_INTS2_OFFSET)  @ This is where the interrupt status can be found (Where the interrupt came from)
    LDR     R1, [R0]                                            @ Load the value from this address
    LDR     R0, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)        @ This is the GPIO address for clearing pending interrupts
    STR     R1, [R0]                                            @ Store the mask representing the button from which the interrupt was called in the given 
                                                                @ address to clear interrupt for that button
    POP     {PC}
@-----------------------------------------------------------------------------------------------------------------------

@ Toggle LED state (Alarm interrupt)
.thumb_func 
toggle_led:
    PUSH    {LR}                    
    @ Check boolean to see if toggle is active
    MOVS    R0, #GPIO_LED_PIN                                   @ Get value of LED Pin
    BL      asm_gpio_get                                        @ Get status of LED Pin
    CMP     R0, #LED_VALUE_OFF                                  @ If the LED is off:
    BEQ     led_set_on                                          @   Branch to led_set_on
led_set_off:                                                    @ Else:
    MOVS    R1, #LED_VALUE_OFF                                  @   Set R1 to value of LED off
    B       led_set_state                                       @   Set the state of the pin to the value of R1
led_set_on:
    MOVS    R1, #LED_VALUE_ON                                   @ Set R1 to value of LED on
led_set_state:
    MOVS    R0, #GPIO_LED_PIN                                   @ Get the value of the LED Pin
    BL      asm_gpio_put                                        @ Set the status of LED Pin
end_toggle:
    @ Clear pending interrupt status
    LDR     R1, =(TIMER_BASE + TIMER_INTR_OFFSET)               @ This is the Timer address for clearing pending interrupts
    MOVS    R0, #1                                              @ Set R0 to 1 for enable bit
    STR     R0, [R1]                                            @ Store 1 in given address to clear interrupt for Alarm0
    POP     {PC}

@------------------------------------------------------------------------
.align 4
msg:                .asciz "Hello World!\n"
flashing_msg:       .asciz "LED Status: Flashing\n"
not_flashing_msg:   .asciz "LED Status: Not Flashing\n"
half_msg:           .asciz "Alarm Delay halved\n"
double_msg:         .asciz "Alarm Delay doubled\n"
reset_msg:          .asciz "Alarm Delay reset\n"

.data
lstate: .word   DFLT_STATE_STRT
ltimer: .word   DFLT_ALARM_TIME
#include "hardware/regs/addressmap.h"
#include "hardware/regs/io_bank0.h"
#include "hardware/regs/timer.h"
#include "hardware/regs/m0plus.h"

.syntax unified                 @ Specify unified assembly syntax
.cpu    cortex-m0plus           @ Specify CPU type is Cortex M0+
.thumb                          @ Specify thumb assembly for RP2040
.global main_asm                @ Provide program starting address to the linker
.align 4                        @ Specify code alignment

.equ    ALRM_ISR_OFFSET, 0x40
.equ    DFLT_ALARM_TIME, 1000000
.equ    DFLT_STATE_STRT, 1

@ Entry point to the ASM portion of the program
main_asm:
    BL      install_alarm_intr
    BL      asm_init_adc

main_loop:
    BL      set_alarm_delay
    WFI
    B       main_loop            @ Infinite loop

@ Set alarm delay
set_alarm_delay:
    PUSH    {LR}

    @ Enable alarm
    LDR     R1, =(TIMER_BASE + TIMER_INTE_OFFSET)       @ Address of the Timer Interrupt Enabler
    LDR     R4, =DFLT_STATE_STRT
    STR     R4, [R1]                                    @ Store value of toggleStatus (R4) in here (either on or off)
    @ Set timer value                           
    LDR     R2, =(TIMER_BASE + TIMER_TIMELR_OFFSET)
    LDR     R1, [R2]
    LDR     R2, =DFLT_ALARM_TIME 
    ADD     R1, R2
    @ Update alarm delay value                                
    LDR     R2, =(TIMER_BASE + TIMER_ALARM0_OFFSET)     @ This is where you set the alarm delay
    STR     R1, [R2]                                    @ Store the updated alarm delay here

    POP     {PC}    

install_alarm_intr:
    PUSH    {LR}

    @ Add ISR address to vector table
    LDR     R0, =(PPB_BASE + M0PLUS_VTOR_OFFSET)            @ This is the base address for the Vector Table
    LDR     R1, [R0]                                        @ Load the value from this address into R1
    MOVS    R0, #ALRM_ISR_OFFSET                            @ Offset to Alarm0 in the Vector Table
    ADD     R0, R1                                          @ Add this offset to the Vector Table value
    LDR     R1, =alarm_isr                                  @ Get the address of the ISR
    STR     R1, [R0]                                        @ Store the address of the ISR in the position of Alarm0 in the Vector Table

    @ Disable interrupt for Alarm
    LDR     R0, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)       @ Address of the NVIC Clear-Pending Register
    MOVS    R1, #1                                          @ Alarm0 is int #0, so set R1 to 1
    STR     R1, [R0]                                        @ Set appropriate bit to 1 to disable the interrupt

    @ Enable interrupt for Alarm
    LDR     R0, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)       @ Address of the NVIC Set-Enable Register
    MOVS    R1, #1                                          @ Alarm0 is int #0, so set R1 to 1
    STR     R1, [R0]                                        @ Set appropriate bit to 1 to enable the interrupt

    POP     {PC}

.thumb_func 
@ Alarm ISR
alarm_isr:
    PUSH    {LR}

    LDR     R0, =alarmCall
    BL      printf

    @ Call to adc reading subroutine                
    BL read_adc    

    @ Clear pending interrupt status
    LDR     R1, =(TIMER_BASE + TIMER_INTR_OFFSET)               @ This is the Timer address for clearing pending interrupts
    MOVS    R0, #1                                              @ Set R0 to 1 for enable bit
    STR     R0, [R1]                                            @ Store 1 in given address to clear interrupt for Alarm0

    POP     {PC}

asm_init_adc:
    PUSH    {LR}

    BL       init_adc
    @LDR     R0, =(ADC_BASE)                                     @ Address for ADC base (control and status register)
    @LDR     R1, [R0]                                            @ Read value of ADC base
    @LDR     R2, =0x3003                                         @ Mask for first Enable and AINSEL=4
    @ORRS    R1, R2                                              @ Mask specified bits to 1
    @LDR     R2, =0xFFE0FFFF                                     @ Mask to disable round-robin
    @ANDS    R1, R2                                              @ Mask round-robin bits to 0
    @STR     R1, [R0]                                            @ Store result in ADC control and status register

    POP     {PC}

read_adc:
    PUSH    {LR}

    @LDR     R0, =(ADC_BASE)
    @LDR     R1, [R0]
    @LDR     R2, =0x4                                            @ Mask start_once bit
    @ORRS    R1, R2
    @STR     R1, [R0]
    @LDR     R1, =(ADC_BASE + 0x04)                              @ Address for result register
    @LDR     R0, [R1]                                            @ Get most recent result
    BL      ADCToCelsius                                        @ Call to C method for converting ADC to Celsius

    POP     {PC}

@ Set data alignment
.data
    alarmCall:          .asciz "\n**ALARM CALL**\n"
    .align 4

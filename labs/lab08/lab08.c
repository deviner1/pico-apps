#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#define PI 3.14159265359    // Comparable value of pi
#define MAX_ITER 100000     // Maximum number of iterations for series (as opposed to infinity)

absolute_time_t startTime;
int count = 0;

// Must declare the main assembly entry point before use.
void main_asm();

// Function to get the enable status of the XIP cache
bool get_xip_cache_en(){
    uint8_t * p = (uint8_t *)(XIP_CTRL_BASE);
    return p[8];
}

// Function to set the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en){
    uint8_t * p = (uint8_t *)(XIP_CTRL_BASE);
    p[0] = cache_en;
    return cache_en;
}

/**
 * @brief Function to set the global variable startTime to the current time since launch
 */
void startTimer(){
    startTime = get_absolute_time();
}

/**
 * @brief Function to return the total time difference in microseconds between startTime and time at function call
 */
int endTimer(){
    uint64_t diff = absolute_time_diff_us(startTime, get_absolute_time());
    return (int)diff;
}

/**
 * @brief Function to return the total time difference in microseconds between startTime and time at function call
 */
void printTime(int time){
    if(count==0)
        printf("Cold cache runtime: %dms\n", time);
    else if(count==1)
        printf("Warm cache runtime: %dms\n", time);
    else
        printf("No cache runtime: %dms\n", time);
    count++;
}

/**
 * @brief LAB #08 - TEMPLATE
 *        Main entry point for the code - calls the main assembly
 *        function where the body of the code is implemented.
 * 
 * @return int      Returns exit-status zero on completion.
 */
int main() {

    stdio_init_all();
    // Jump into the main assembly code subroutine.
    main_asm();

    // Returning zero indicates everything went okay.
    return 0;
}

#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#define PI 3.14159265359    // Comparable value of pi
#define MAX_ITER 100000     // Maximum number of iterations for series (as opposed to infinity)

// NOTE: json file changed due to error with Wowki functionality (line 11: changed "env" to "builder" for pico-sdk)

 /**
 * floatPi Function:
 * Approximates pi with single precision (float) and prints summary to console
 *
 * @param int numIter - number of iterations over which pi is estimated using Wallis product algorithm
 *
 * @return float - approximation of Pi.
 */

float floatPi(int numIter){
    float pi = 1;

    /** consider a to be the primary component of Wallis product algorithm
     *  i.e (  a       a  )
     *      (  -   *   -  ) * ...
     *      ((a-1)   (a+1))
	 */

    float a; /**< the initial value of 2n in the Wallis product series */
    float x; /**< this will be the first fraction (a/(a-1)) */
    float y; /**< this will be the second fraction (a/(a+1)) */
    float z; /**< this will be the multiplier (x*y) */

    // Approximate pi/2
    for(int i=1; i<numIter; i++){
        a = 2*i;
        x = (a/(a-1));
        y = (a/(a+1));
	    z = (x*y);
        pi = pi * z;
    }

    pi = pi * 2;
    float diff = (PI>pi)? PI-pi:pi-PI;/**< this ensures that a positive value of difference is obtained (had issues with 
                                                                                                                Math.abs())*/
    float pc_error = 100*(diff/PI); /**< this is the percentage error of the difference between expected and calculated Pi */
    
    printf("Apx pi value: %f\nDifference: %f\nApx error: %f%%\n\n",pi,diff,pc_error);
	
	return pi;
}

/**
 * doublePi Function:
 * Approximates pi with double precision (double) and prints summary to console
 *
 * @param int numIter - number of iterations over which pi is estimated using Wallis product algorithm
 *
 * @return float - approximation of Pi.
 */

double doublePi(int numIter){
    double pi = 1;

    /** consider a to be the primary component of Wallis product algorithm
     *  i.e (  a       a  )
     *      (  -   *   -  ) * ...
     *      ((a-1)   (a+1))
	 */

    double a; /**< the initial value of 2n in the Wallis product series */
    double x; /**< this will be the first fraction (a/(a-1)) */
    double y; /**< this will be the second fraction (a/(a+1)) */
    double z; /**< this will be the multiplier (x*y) */

    // Approximate pi/2
    for(int i=1; i<=numIter; i++){
        a = 2*i;
        x = (a/(a-1));
        y = (a/(a+1));
		z = (x*y);
        pi = pi * z;
    }

    pi = pi * 2;
    double diff = (PI>pi)? PI-pi:pi-PI;/**< this ensures that a positive value of difference is obtained (had issues with 
                                                                                                    Math.abs())*/
    double pc_error = 100*(diff/PI); /**< this is the percentage error of the difference between expected and calculated Pi */
    
    printf("Apx pi value: %f\nDifference: %f\nApx error: %f%%\n\n",pi,diff,pc_error);
	
	return pi;
}

/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

int main() {
    
    // Line required for terminal output with Wowki: initialises IO
    stdio_init_all();

    // Print a console message to inform user what's going on.
    printf("**Approximating Pi!**\n");

	// Print to indicate floatPi function is being entered
    printf("Float summary:\n");
    floatPi(MAX_ITER);
	
	// Print to indicate doublePi function is being entered
    printf("Double summary:\n");
    doublePi(MAX_ITER);
	
    // Returning zero indicates everything went okay.
    return 0;
}
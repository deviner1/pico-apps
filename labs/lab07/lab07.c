#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "pico/multicore.h"
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#define PI 3.14159265359    // Comparable value of pi
#define MAX_ITER 100000     // Maximum number of iterations for series (as opposed to infinity)

void core1_entry() {
    while (1) {
        // Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

// Function to get the enable status of the XIP cache
bool get_xip_cache_en(){
    uint8_t * p = (uint8_t *)(XIP_CTRL_BASE);
    return p[8];
}
// Function to set the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en){
    uint8_t * p = (uint8_t *)(XIP_CTRL_BASE);
    p[0] = cache_en;
    return cache_en;
}

/**
 * clock Function:
 * Pico SDK implementation of time.h clock method (returns clock ticks since startup)
 *
 * @return clock_t - clock ticks since startup
 */

clock_t clock()
{
    return (clock_t) time_us_64() / 10000;
}

 /**
 * floatPi Function:
 * Approximates pi with single precision (float) and prints summary to console
 *
 * @param int numIter - number of iterations over which pi is estimated using Wallis product algorithm
 *
 * @return float - approximation of Pi.
 */

float floatPi(int numIter){
    clock_t startTime = clock(); 
    printf("Float summary:\n");
    float pi = 1;

    /** consider a to be the primary component of Wallis product algorithm
     *  i.e (  a       a  )
     *      (  -   *   -  ) * ...
     *      ((a-1)   (a+1))
	 */

    float a; /**< the initial value of 2n in the Wallis product series */
    float x; /**< this will be the first fraction (a/(a-1)) */
    float y; /**< this will be the second fraction (a/(a+1)) */
    float z; /**< this will be the multiplier (x*y) */

    // Approximate pi/2
    for(int i=1; i<numIter; i++){
        a = 2*i;
        x = (a/(a-1));
        y = (a/(a+1));
	    z = (x*y);
        pi = pi * z;
    }

    pi = pi * 2;
    float diff = (PI>pi)? PI-pi:pi-PI;/**< this ensures that a positive value of difference is obtained (had issues with 
                                                                                                                Math.abs())*/
    float pc_error = 100*(diff/PI); /**< this is the percentage error of the difference between expected and calculated Pi */
    
    printf("Apx pi value: %f\nDifference: %f\nApx error: %f%%\n\n",pi,diff,pc_error);
    clock_t endTime = clock();

    double executionTime = (double)(endTime-startTime)/CLOCKS_PER_SEC;
    printf("Float execution time: %f\n\n", executionTime);
	
	return pi;
}

/**
 * doublePi Function:
 * Approximates pi with double precision (double) and prints summary to console
 *
 * @param int numIter - number of iterations over which pi is estimated using Wallis product algorithm
 *
 * @return float - approximation of Pi.
 */

double doublePi(int numIter){
    clock_t startTime = clock(); 
    printf("Double summary:\n"); 
    double pi = 1;

    /** consider a to be the primary component of Wallis product algorithm
     *  i.e (  a       a  )
     *      (  -   *   -  ) * ...
     *      ((a-1)   (a+1))
	 */

    double a; /**< the initial value of 2n in the Wallis product series */
    double x; /**< this will be the first fraction (a/(a-1)) */
    double y; /**< this will be the second fraction (a/(a+1)) */
    double z; /**< this will be the multiplier (x*y) */

    // Approximate pi/2
    for(int i=1; i<=numIter; i++){
        a = 2*i;
        x = (a/(a-1));
        y = (a/(a+1));
		z = (x*y);
        pi = pi * z;
    }

    pi = pi * 2;
    double diff = (PI>pi)? PI-pi:pi-PI;/**< this ensures that a positive value of difference is obtained (had issues with 
                                                                                                    Math.abs())*/
    double pc_error = 100*(diff/PI); /**< this is the percentage error of the difference between expected and calculated Pi */
    
    printf("Apx pi value: %f\nDifference: %f\nApx error: %f%%\n\n",pi,diff,pc_error);
	
    clock_t endTime = clock();

    double executionTime = (double)(endTime-startTime)/CLOCKS_PER_SEC;
    printf("Double execution time: %f\n\n", executionTime); 

	return pi;
}

/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

int main() {
    //SINGLE-CORE NO CACHE CODE
    set_xip_cache_en(0);
    clock_t startup = clock();

    // Line required for terminal output with Wowki: initialises IO
    stdio_init_all();
    // Print a console message to inform user what's going on.
    printf("**Approximating Pi!**\n");

	// Print to indicate floatPi function is being entered
    floatPi(MAX_ITER);
	
	// Print to indicate doublePi function is being entered
    doublePi(MAX_ITER);

    clock_t end = clock();
    double totalTime = (double)(end-startup)/CLOCKS_PER_SEC;
    printf("Total Single-Core application (with no cache) execution time : %f\n\n", totalTime);

    printf("-----------------------------------------------------------------------------\n\n");

    //SINGLE-CORE CACHE CODE
    set_xip_cache_en(1);
    startup = clock();

    // Line required for terminal output with Wowki: initialises IO
    stdio_init_all();
    // Print a console message to inform user what's going on.
    printf("**Approximating Pi!**\n");

	// Print to indicate floatPi function is being entered
    floatPi(MAX_ITER);
	
	// Print to indicate doublePi function is being entered
    doublePi(MAX_ITER);

    end = clock();
    totalTime = (double)(end-startup)/CLOCKS_PER_SEC;
    printf("Total Single-Core applicatiom (with cache) execution time : %f\n\n", totalTime);

    printf("-----------------------------------------------------------------------------\n\n");

    //DUAL-CORE NO CACHE CODE
    set_xip_cache_en(0);
    startup = clock();

    multicore_launch_core1(core1_entry);

    multicore_fifo_push_blocking((uintptr_t) &doublePi);
    multicore_fifo_push_blocking(MAX_ITER);

    floatPi(MAX_ITER);
    multicore_fifo_pop_blocking();

    end = clock();
    totalTime = (double)(end-startup)/CLOCKS_PER_SEC;
    printf("Total Dual-Core application (with no cache) execution time: %f\n\n", totalTime);

    printf("-----------------------------------------------------------------------------\n\n");

    //DUAL-CORE CACHE CODE
    set_xip_cache_en(1);
    startup = clock();

    multicore_fifo_push_blocking((uintptr_t) &doublePi);
    multicore_fifo_push_blocking(MAX_ITER);

    floatPi(MAX_ITER);
    multicore_fifo_pop_blocking();

    end = clock();
    totalTime = (double)(end-startup)/CLOCKS_PER_SEC;
    printf("Total Dual-Core application (with cache) execution time: %f\n\n", totalTime);

    // Returning zero indicates everything went okay.
    return 0;
}
